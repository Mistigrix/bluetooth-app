import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:bluetooth_app/command_page.dart';
import 'package:bluetooth_app/models/deviceModel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import 'package:flutter_blue/flutter_blue.dart' as flutter_blue;

class BluetoothPage extends StatefulWidget {
  const BluetoothPage({super.key});

  @override
  State<BluetoothPage> createState() => _BluetoothPageState();
}

class _BluetoothPageState extends State<BluetoothPage> {
  late String enabled;
  FlutterBluetoothSerial bluetooth = FlutterBluetoothSerial.instance;
  flutter_blue.FlutterBlue flutterBlue = flutter_blue.FlutterBlue.instance;
  //late Future<bool> isEnabled;
  late Stream bluetoothScan;
  late BluetoothState _bluetoothState;
  late bool _apairing = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    FlutterBluetoothSerial.instance.state.then((state) {
      setState(() {
        _bluetoothState = state;
      });
    });

    FlutterBluetoothSerial.instance.onStateChanged().listen((state) {
      setState(() {
        _bluetoothState = state;
      });
    });
    //devices = getDevices();
    //startScan();
  }

  List<BluetoothDevice> devices = [];

  startScan() {
    setState(() {
      devices.clear();
    });
    FlutterBluetoothSerial.instance.startDiscovery().listen((Sdevice) {
      var device = Sdevice.device;
      // Vérifier si l'appareil n'est pas déjà apparié

      setState(() {
        devices.add(device);
        print(
            'Nouvel appareil découvert : ${device.name}, Adresse : ${device.address}');
      });
    });
  }

  connectToDevice(BluetoothDevice device) async {
    BluetoothConnection connection =
        await BluetoothConnection.toAddress(device.address);

    //connection

    if (connection.isConnected) {
      print("Connection etablit");
    } else {
      print("Echec de la connexion");
    }
  }

  getIsEnabled() {
    Future<bool?> isEnabled = bluetooth.isEnabled;

    if (isEnabled == true) {
      print("Bluetooth Actif");
    } else {
      print("Bluetooth Desactiver");
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //getIsEnabled();
    return Scaffold(
      appBar: AppBar(title: Text("Bluetooth disponible")),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            ElevatedButton(onPressed: startScan, child: Text("Scann")),
            Row(
                //mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Ajout d'un Nouvel Appareil",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ]),
            !_bluetoothState.isEnabled
                ? Center(
                    child: Column(
                      children: [
                        Icon(
                          Icons.bluetooth_disabled,
                          size: 200,
                          color: Colors.grey,
                        ),
                        Text(
                          "Activez le bluetooth pour trouver des appareils à proximité",
                          textAlign: TextAlign.center,
                        ),
                        ElevatedButton(
                            onPressed: () async {
                              await FlutterBluetoothSerial.instance
                                  .requestEnable();
                            },
                            child: Text("Activer le Bluetooth")),
                      ],
                    ),
                  )
                : SizedBox(
                    width: MediaQuery.of(context).size.width,
                    height: 500,
                    child: ListView.builder(
                        itemCount: devices.length,
                        itemBuilder: (itemBuilder, index) {
                          BluetoothDevice device = devices[index];
                          return ListTile(
                            title: Text(device.name.toString()),
                            subtitle: Text(device.address),
                            //Text(discoveredDevices[index].address),
                            leading: const Icon(Icons.bluetooth_drive),
                            onTap: () => AwesomeDialog(
                              context: context,
                              dialogType: DialogType.noHeader,
                              dismissOnBackKeyPress: false,
                              body: Column(
                                children: [
                                  const Center(
                                      child: Text(
                                    "Voulez-vous ajouter l'appareil",
                                    style: TextStyle(
                                      fontSize: 20,
                                    ),
                                  )),
                                  Column(
                                    children: [
                                      Center(
                                          child: Text(
                                        device.name.toString(),
                                        style: TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.bold),
                                      )),
                                      _apairing
                                          ? Center(
                                              child:
                                                  CircularProgressIndicator())
                                          : Text("")
                                    ],
                                  ),
                                ],
                              ),
                              btnCancelOnPress: () {
                                setState(() {
                                  _apairing = false;
                                });
                              },
                              btnOkOnPress: () async {
                                setState(() {
                                  _apairing = true;
                                });

                                BluetoothConnection bluetoothConnection =
                                    await BluetoothConnection.toAddress(
                                        device.address);

                                if (bluetoothConnection.isConnected) {
                                  setState(() {
                                    _apairing = false;
                                  });

                                  print("Connecter à ${device.name}");
                                  // ignore: use_build_context_synchronously
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (builder) => CommandPage(
                                                device: DeviceModel(
                                                    name:
                                                        device.name.toString(),
                                                    type: 1,
                                                    addMacAppareil:
                                                        "9C:9C:1F:E9:E5:54"),
                                                bluetoothConnection:
                                                    bluetoothConnection,
                                              )));
                                } else {
                                  setState(() {
                                    _apairing = false;
                                  });
                                  print(
                                      "Impossible de se connecter à l'appareil");
                                }
                              },
                            )..show(),
                            onLongPress: () {
                              print("Hello this is a bublb");
                            },
                          );
                        }),
                  ),
          ],
        ),
      ),
    );
  }
}
