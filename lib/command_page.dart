import 'dart:convert';

import 'package:bluetooth_app/models/deviceModel.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';

class CommandPage extends StatefulWidget {
  const CommandPage({
    super.key,
    required this.device,
    this.bluetoothConnection,
  });

  final DeviceModel device;
  final BluetoothConnection? bluetoothConnection;

  @override
  State<CommandPage> createState() => _CommandPageState();
}

class _CommandPageState extends State<CommandPage> {
  late TextEditingController _controller;
  late TextEditingController _deviceName_controller;
  late String _value = 'WIFI ODC';
  late Image _deviceImg = Image.asset("");
  late bool next = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // WiFiForIoTPlugin.connect(widget.apName,
    //     password: widget.apPassword,
    //     joinOnce: true,
    //     security: NetworkSecurity.WPA);

    _controller = TextEditingController();
    _deviceName_controller = TextEditingController();
    _deviceName_controller.text = widget.device.name;
  }

  sendData(String data) {
    Uint8List encodedData = Uint8List.fromList(utf8.encode(data));
    widget.bluetoothConnection?.output.add(encodedData);

    widget.bluetoothConnection?.output.allSent;

    setState(() {
      next = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          padding: const EdgeInsets.only(top: 50, left: 30, right: 30),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                  onPressed: () {
                    sendData("1");
                  },
                  child: Text("Allumer")),
              ElevatedButton(
                  onPressed: () {
                    sendData("0");
                  },
                  child: Text("Eteindre")),
            ],
          )),
    );
  }

  // ignore: non_constant_identifier_names
  Widget RenameDevice() {
    return Column(
      children: [
        const Padding(padding: EdgeInsets.only(top: 50)),
        const Text(
          "Renomer l'appareil",
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
        ),
        const Text(
          "Choisissez un nom explicite qui vous donnera une bref d'excription de l'endroit ou ce trouve votre équipement",
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 15),
        ),
        const Padding(padding: EdgeInsets.only(bottom: 20)),
        TextField(
          controller: _deviceName_controller,
          decoration: const InputDecoration(),
        ),
        ElevatedButton(onPressed: () async {}, child: const Text("Terminer"))
      ],
    );
  }

  Widget SelectConnexion() {
    return Column(
      children: [
        const Text(
          "Sélectionner le réseau Wi-Fi",
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 20),
        ),
        const Padding(padding: EdgeInsets.only(bottom: 20)),
        SizedBox(
          width: 300,
          height: 50,
          child: DropdownButtonFormField(
            items: const [
              DropdownMenuItem(
                value: 'WIFI ODC',
                child: Text("WIFI ODC"),
              ),
              DropdownMenuItem(
                value: 'connectedbulb',
                child: Text("ConnectedBulb"),
              ),
              DropdownMenuItem(
                value: 'wifi_tva',
                child: Text("WIFI_TVA"),
              ),
            ],
            onChanged: (value) {
              setState(() {
                _value = value!;
              });
            },
            value: 'WIFI ODC',
          ),
        ),
        const Padding(padding: EdgeInsets.only(bottom: 50)),
        SizedBox(
          height: 50,
          width: 300,
          child: TextField(
            controller: _controller,
            decoration:
                const InputDecoration(hintText: "Saisir le mot de passe wifi"),
          ),
        ),
        ElevatedButton(
          child: Text("Continuer"),
          onPressed: () {
            print(_value);

            if (widget.bluetoothConnection != null) {
              Uint8List encodedData = Uint8List.fromList(
                  utf8.encode(_value + ":" + _controller.text));
              widget.bluetoothConnection?.output.add(encodedData);

              widget.bluetoothConnection?.output.allSent;

              setState(() {
                next = true;
              });
            }
          },
        )
      ],
    );
  }
}
