class DeviceModel {
  final String name;
  final int type;
  final String addMacAppareil;


  DeviceModel({required this.name, required this.type, required this.addMacAppareil});

  factory DeviceModel.fromJson(Map<String, String> json) => DeviceModel(
        name: json['nomAppareil'].toString(),
        type: int.parse(json['idType']!),
        addMacAppareil: json['addMacAppareil'].toString(),

      );
  

  // factory Device.string(String string) => Device(
  //       name: json['nom'].toString(),
  //       type: json['prenom'].toString(),
  //     );
      
}
